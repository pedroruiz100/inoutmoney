package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.UsuarioDao;
import py.com.local.control.money.core.model.Usuario;
import py.com.local.control.money.core.services.UsuarioService;

@Service("usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDao usuDao;
	
	@Override
	public List<Usuario> listarTodos() {
		// TODO Auto-generated method stub
		return usuDao.listarTodos();
	}

	@Override
	public Optional<Usuario> getById(long id) {
		// TODO Auto-generated method stub
		return usuDao.getById((long) id);
	}

	@Override
	public Optional<Usuario> getByUsuario(String usu) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(Usuario usu) {
		// TODO Auto-generated method stub
		if (usu.getId() == null)
			usuDao.insertar(usu);
		else
			usuDao.actualizar(usu);
	}

	@Override
	public void actualizar(Usuario usu) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		usuDao.borrarPorId(id);
	}

	@Override
	public boolean isExisteUsuario(Usuario usu) {
		// TODO Auto-generated method stub
		try {
			return getById(usu.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<Usuario> listAllByEstado() {
		// TODO Auto-generated method stub
		return usuDao.listAllByEstado();
	}

	@Override
	public Usuario getByUsuClave(String usu, String clave) {
		// TODO Auto-generated method stub
		return usuDao.getByUsuClave(usu, clave);
	}

}
