package py.com.local.control.money;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import py.com.local.control.money.JpaConfiguration;

@SpringBootApplication(scanBasePackages={"py.com.local.control.money.web.controllers",
		"py.com.local.control.money.core.services","py.com.local.control.money.core.dao"})
@Import(JpaConfiguration.class)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
