package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Usuario;

public interface UsuarioService {

	List<Usuario> listarTodos();

	Optional<Usuario> getById(long id);

	Optional<Usuario> getByUsuario(String usu);

	void insertar(Usuario usu);

	void actualizar(Usuario usu);

	void borrarPorId(Long id);
	
	boolean isExisteUsuario(Usuario usu);
	
	List<Usuario> listAllByEstado();

	Usuario getByUsuClave(String usu, String clave);
}
