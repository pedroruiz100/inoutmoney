package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Frecuencia;
import py.com.local.control.money.core.services.FrecuenciaService;
import py.com.local.control.money.util.ErrorDTO;


@RestController
@RequestMapping("/frecuencia")
@CrossOrigin
public class FrecuenciaController {

	public static final Logger logger = LoggerFactory.getLogger(FrecuenciaController.class);
	@Autowired
	private FrecuenciaService freSrv;

	// ================ RECUPERAMOS TODOS LOS Frecuencia ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarFrecuencias() {
		List<Frecuencia> frecu = freSrv.listarTodos();
		if (frecu.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Frecuencia>>(frecu, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODAS LAS FRECUENCIAS ACTIVAS================
		@SuppressWarnings("rawtypes")
		@RequestMapping(value = "/", method = RequestMethod.GET)
		public ResponseEntity<?> listarFrecuActivas() {
			List<Frecuencia> frecu = freSrv.listAllByEstado();
			if (frecu.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
				// podríamos retornar también HttpStatus.NOT_FOUND
			}
			return new ResponseEntity<List<Frecuencia>>(frecu, HttpStatus.OK);
		}

	// ================ RECUPERAMOS UNA Frecuencia A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getFrecuencia(@PathVariable("id") long id) {
		logger.info("Vamos a obtener la Frecuencia con id {}.", id);
		Optional<Frecuencia> x = freSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningúna frecuencia con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningúna Frecuencia con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Frecuencia>(x.get(), HttpStatus.OK);
		}

	}
	
	// ================ CREAMOS UNA Frecuencia ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearFrecuencia(@RequestBody Frecuencia frecu, UriComponentsBuilder ucBuilder) {
		logger.info("Creando la Frecuencia : {}", frecu);
		if (freSrv.isExisteFrecuencia(frecu)) {
			logger.error("Inserción fallida. Ya existe un registro con la Frecuencia {}", frecu.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con la Frecuencia " + frecu.getId()),
					HttpStatus.CONFLICT);
		}
		freSrv.guardarFrecuencia(frecu);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/frecuencia/{id}").buildAndExpand(frecu.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	// ================ ACTUALIZAMOS LOS DATOS DE UNA Frecuencia ================
		@RequestMapping(value = "/", method = RequestMethod.PUT)
		public ResponseEntity<?> actualizarFrecuencia(@RequestBody Frecuencia frecu) {
			logger.info("Actualizando la Frecuencia con id {}", frecu.getId());
			Optional<Frecuencia> u = freSrv.getById(frecu.getId());
			if (u == null) {
				logger.error("Actualización fallida. No existe la Frecuencia con el id {}.", frecu.getId());
				return new ResponseEntity<ErrorDTO>(
						new ErrorDTO("Actualización fallida. No existe la Frecuencia con el id " + frecu.getId()), HttpStatus.NOT_FOUND);
			}
			Frecuencia frecuBD = u.get();
			frecuBD.setDescripcion(frecu.getDescripcion());
			frecuBD.setEstado(frecu.getEstado());
			freSrv.guardarFrecuencia(frecuBD);
			return new ResponseEntity<Frecuencia>(frecuBD, HttpStatus.OK);
		}
		
		// ================ ELIMINAMOS UNAnFrecuencia ================ 
		@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
		public ResponseEntity<?> eliminarFrecuencia(@PathVariable("id") long id) {
			logger.info("Obteniendo y eliminando la Frecuencia con id {}", id);
			Optional<Frecuencia> frecu = freSrv.getById(id);
			if (!frecu.isPresent()) {
				logger.error("Eliminación fallida. No existe la Frecuencia con el id {}", id);
				return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe la Frecuencia con el id " + id),
						HttpStatus.NOT_FOUND);
			}
			freSrv.borrarPorId(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
			
}
