package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Usuario;


public interface UsuarioDao {

List<Usuario> listarTodos();
	
	Optional<Usuario> getById(long id);
	
	Optional<Usuario> getByUsuario (String usu);

	void insertar(Usuario usu);

	void actualizar(Usuario usu);

	void borrarPorId(Long id);
	
	List<Usuario> listAllByEstado();
	
	Usuario getByUsuClave(String usu, String clave);
	
}
