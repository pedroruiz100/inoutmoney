package py.com.local.control.money.core.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Movimiento;

public interface MovimientoDao {

	List<Movimiento> listarTodos();

	Optional<Movimiento> getById(long id);

	Optional<Movimiento> getByDescri(String descri);
	
	Optional<Movimiento> getByFecha(Timestamp fecha);

	void insertar(Movimiento movimiento);

	void actualizar(Movimiento movimiento);

	void borrarPorId(Long id);
	
	List<Movimiento> listAllByEstado();
	
}
