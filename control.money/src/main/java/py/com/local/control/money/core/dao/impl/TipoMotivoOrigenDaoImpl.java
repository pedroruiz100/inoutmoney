package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.TipoMotivoOrigenDao;
import py.com.local.control.money.core.model.TipoMotivoOrigen;

@Repository("tipoMotivoOrigenDao")
public class TipoMotivoOrigenDaoImpl extends AbstractDao<TipoMotivoOrigen> implements TipoMotivoOrigenDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoMotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		List<TipoMotivoOrigen> tmo = getEntityManager().createNamedQuery("TipoMotivoOrigen.findAll").getResultList();
		return tmo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoMotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		List<TipoMotivoOrigen> tmo = getEntityManager().createNamedQuery("TipoMotivoOrigen.findByEstadoAll").getResultList();
		return tmo;
	}
	
	@Override
	public Optional<TipoMotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		Optional<TipoMotivoOrigen> tmo = super.getById(id);
		return tmo;
	}

	@Override
	public Optional<TipoMotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoMotivoOrigen tipoMotiOri) {
		// TODO Auto-generated method stub
		super.persistir(tipoMotiOri);

	}

	@Override
	public void actualizar(TipoMotivoOrigen tipoMotiOri) {
		// TODO Auto-generated method stub
		super.actualizar(tipoMotiOri);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		TipoMotivoOrigen tmo = (TipoMotivoOrigen) getEntityManager().createQuery("DELETE FROM TipoMovimientoOrigen WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

}
