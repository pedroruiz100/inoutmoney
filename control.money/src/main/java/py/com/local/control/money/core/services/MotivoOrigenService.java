package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.MotivoOrigen;

public interface MotivoOrigenService {

	List<MotivoOrigen> listarTodos();

	Optional<MotivoOrigen> getById(long id);

	Optional<MotivoOrigen> getByDescri(String descri);

	void insertar(MotivoOrigen motiOri);

	void actualizar(MotivoOrigen motiOri);

	void borrarPorId(Long id);
	
	boolean isExisteMotivoOrigen(MotivoOrigen motiOri);

	List<MotivoOrigen> listAllByEstado();
}
