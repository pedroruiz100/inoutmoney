package py.com.local.control.money.core.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


/**
 * The persistent class for the "usuarios" database table.
 * 
 */
@Entity
@Table(name="\"usuarios\"")
//@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
@NamedQueries({
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u"),
@NamedQuery(name="Usuario.findByEstadoAll", query="SELECT u FROM Usuario u where u.estado=true")
})
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Usuario\"")
	private String usuario;
	
	@Column(name="\"Apellido\"")
	private String apellido;

	@Column(name="\"Clave\"")
	private String clave;

	@Column(name="\"Correo\"")
	private String correo;

	@Column(name="\"Documento\"")
	private String documento;

	@Column(name="\"Nombre\"")
	private String nombre;

	@Column(name="\"Estado\"")
	private boolean estado;

	@ManyToMany
	@NotEmpty
	@JoinTable(
	name="usuarios_perfiles"
	, joinColumns={
	@JoinColumn(name="id_usuario")
	}
	, inverseJoinColumns={
	@JoinColumn(name="id_perfil")
	}
	)
	
	private List<Perfil> perfiles;
	

	public Usuario() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDocumento() {
		return this.documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	public List<Perfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<Perfil> perfiles) {
		this.perfiles = perfiles;
	}

}