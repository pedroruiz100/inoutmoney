package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "tipos_motivos_origenes" database table.
 * 
 */
@Entity
@Table(name="\"tipos_motivos_origenes\"")
//@NamedQuery(name="TipoMotivoOrigen.findAll", query="SELECT t FROM TipoMotivoOrigen t")
@NamedQueries({
@NamedQuery(name="TipoMotivoOrigen.findAll", query="SELECT t FROM TipoMotivoOrigen t"),
@NamedQuery(name="TipoMotivoOrigen.findByEstadoAll", query="SELECT t FROM TipoMotivoOrigen t where t.estado=true")
})
public class TipoMotivoOrigen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Descripcion\"")
	private String descripcion;

	@Column(name="\"Estado\"")
	private Boolean estado;
	
	public TipoMotivoOrigen() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}