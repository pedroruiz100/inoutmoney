package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "perfiles" database table.
 * 
 */
@Entity
@Table(name="\"perfiles\"")
//@NamedQuery(name="Perfil.findAll", query="SELECT p FROM Perfil p")
@NamedQueries({
@NamedQuery(name="Perfil.findAll", query="SELECT p FROM Perfil p"),
@NamedQuery(name="Perfil.findByEstadoAll", query="SELECT p FROM Perfil p where p.estado=true")
})
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Descripcion\"")
	private String descripcion;

	@Column(name="\"Estado\"")
	private Boolean estado;
	
	public Perfil() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}