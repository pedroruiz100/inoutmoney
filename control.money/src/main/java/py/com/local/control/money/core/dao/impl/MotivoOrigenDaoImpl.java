package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.MotivoOrigenDao;
import py.com.local.control.money.core.model.MotivoOrigen;

@Repository("motivoOrigenDao")
public class MotivoOrigenDaoImpl extends AbstractDao<MotivoOrigen> implements MotivoOrigenDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<MotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		List<MotivoOrigen> mo = getEntityManager().createNamedQuery("MotivoOrigen.findAll").getResultList();
		return mo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		List<MotivoOrigen> mo = getEntityManager().createNamedQuery("MotivoOrigen.findByEstadoAll").getResultList();
		return mo;
	}
	
	@Override
	public Optional<MotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		Optional<MotivoOrigen> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<MotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		super.persistir(motiOri);

	}

	@Override
	public void actualizar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		super.actualizar(motiOri);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		//super.eliminar(super.getById(id).get());
//		MotivoOrigen mo = (MotivoOrigen) getEntityManager().createQuery("DELETE FROM MotivoOrigen WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);

		super.eliminar(super.getById(id).get());
	}

}
