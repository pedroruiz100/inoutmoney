package py.com.local.control.money.core.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.MovimientoDao;
import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.services.MovimientoService;

@Service("movimientoService")
@Transactional
public class MovimientoServiceImpl implements MovimientoService {

	@Autowired
	private MovimientoDao moviDao;

	@Override
	public List<Movimiento> listarTodos() {
		// TODO Auto-generated method stub
		return moviDao.listarTodos();
	}

	@Override
	public Optional<Movimiento> getById(long id) {
		// TODO Auto-generated method stub
		return moviDao.getById((long) id);
	}

	@Override
	public Optional<Movimiento> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Movimiento> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		return moviDao.getByFecha((Timestamp) fecha);
	}

	@Override
	public void insertar(Movimiento movimiento) {
		// TODO Auto-generated method stub
		if (movimiento.getId() == null)
			moviDao.insertar(movimiento);
		else
			moviDao.actualizar(movimiento);

	}

	@Override
	public void actualizar(Movimiento movimiento) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		moviDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteMovimiento(Movimiento movimiento) {
		// TODO Auto-generated method stub
		try {
			return getById(movimiento.getId()).isPresent();
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public List<Movimiento> listAllByEstado() {
		// TODO Auto-generated method stub
		/*List<Movimiento> movList = new ArrayList<>();
		for (Movimiento mov : moviDao.listAllByEstado()) {
			mov = mov.recuperarSinRecursividad();
		}*/
		return moviDao.listAllByEstado();
	}

}
