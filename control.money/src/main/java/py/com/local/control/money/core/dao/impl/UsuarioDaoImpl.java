package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.UsuarioDao;
import py.com.local.control.money.core.model.Usuario;

@Repository("usuarioDao")
public class UsuarioDaoImpl extends AbstractDao<Usuario> implements UsuarioDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> listarTodos() {
		List<Usuario> usu = getEntityManager().createNamedQuery("Usuario.findAll").getResultList();
		return usu;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Usuario> usu = getEntityManager().createNamedQuery("Usuario.findByEstadoAll").getResultList();
		return usu;
	}

	@Override
	public Optional<Usuario> getById(long id) {
		Optional<Usuario> u = super.getById(id);
		return u;
	}

//	@Override
//	public Optional<Usuario> getByUsuario(String usu) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Usuario> getByUsuario(String usu) {
		// TODO Auto-generated method stub
		logger.debug("Usuario: " + usu);
		try {
			Optional<Usuario> u = Optional.ofNullable(
					(Usuario) getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.usuario = :usu")
							.setParameter("usuario", usu).getSingleResult());
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
		} catch (NoResultException ex) {
			return Optional.empty();
		}
	}

	@Override
	public void insertar(Usuario usu) {
		// TODO Auto-generated method stub
		usu.setUsuario(usu.getUsuario().toLowerCase());
		super.persistir(usu);
	}

	@Override
	public void actualizar(Usuario usu) {
		usu.setUsuario(usu.getUsuario().toLowerCase());
		super.actualizar(usu);
	}

	@Override
	public void borrarPorId(Long id) {
//		Usuario usu = (Usuario) getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

	@Override
	public Usuario getByUsuClave(String usu, String clave) {
		// TODO Auto-generated method stub
		logger.debug("Usuaruio usu: " + usu);
		try {
			Usuario u = (Usuario) getEntityManager()
					.createQuery(
							"SELECT u FROM Usuario u WHERE (u.usuario = :usu OR u.correo= :usu) AND u.clave = :clave")
					.setParameter("usu", usu.toLowerCase())
					.setParameter("clave", clave)
					.getSingleResult();
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
		} catch (NoResultException ex) {
			return null;
		}
	}

}
