package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.TipoMotivoOrigen;
import py.com.local.control.money.core.services.TipoMotivoOrigenService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/tipomotivo")
@CrossOrigin
public class TipoMotivoOrigenController {

	public static final Logger logger = LoggerFactory.getLogger(TipoMotivoOrigenController.class);

	@Autowired
	private TipoMotivoOrigenService tMoSrv;

	// ================ RECUPERAMOS TODOS LOS Tipo ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoMotivoOrigen() {
		List<TipoMotivoOrigen> tMo = tMoSrv.listarTodos();
		if (tMo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoMotivoOrigen>>(tMo, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS TIPO ACTIVO ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoMotivoOrigenActivo() {
		List<TipoMotivoOrigen> tMo = tMoSrv.listAllByEstado();
		if (tMo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoMotivoOrigen>>(tMo, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS UN Tipo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTipoMotivoOrigen(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Tipo con id {}.", id);
		Optional<TipoMotivoOrigen> x = tMoSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Tipo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Tipo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<TipoMotivoOrigen>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN Tipo ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearTipoMotivoOrigen(@RequestBody TipoMotivoOrigen tMo, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Tipo : {}", tMo);
		if (tMoSrv.isExisteTipMotiOri(tMo)) {
			logger.error("Inserción fallida. Ya existe un registro con el Tipo {}", tMo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Tipo " + tMo.getId()),
					HttpStatus.CONFLICT);
		}
		tMoSrv.insertar(tMo);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tipomotivo/{id}").buildAndExpand(tMo.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Tipo ================
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarTipoMotivo(@RequestBody TipoMotivoOrigen tMo) {
		logger.info("Actualizando el Tipo con id {}", tMo.getId());
		Optional<TipoMotivoOrigen> u = tMoSrv.getById(tMo.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Tipo con el id {}.", tMo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Tipo con el id " + tMo.getId()), HttpStatus.NOT_FOUND);
		}
		TipoMotivoOrigen tMoBD = u.get();
		tMoBD.setDescripcion(tMo.getDescripcion());
		tMoBD.setEstado(tMo.getEstado());
		tMoSrv.insertar(tMoBD);
		return new ResponseEntity<TipoMotivoOrigen>(tMoBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Tipo ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoMotivo(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Tipo con id {}", id);
		Optional<TipoMotivoOrigen> tMo = tMoSrv.getById(id);
		if (!tMo.isPresent()) {
			logger.error("Eliminación fallida. No existe el Tipo con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el Tipo con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		tMoSrv.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
