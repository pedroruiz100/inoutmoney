package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.MotivoOrigenDao;
import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.services.MotivoOrigenService;

@Service("MotivoOrigenService")
@Transactional
public class MotivoOrigenServiceImpl implements MotivoOrigenService {

	@Autowired
	private MotivoOrigenDao motiOriDao;
	
	@Override
	public List<MotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodos();
	}

	@Override
	public Optional<MotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		return motiOriDao.getById((long) id);
	}

	@Override
	public Optional<MotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		if (motiOri.getId() == null)
			motiOriDao.insertar(motiOri);
		else
			motiOriDao.actualizar(motiOri);

	}

	@Override
	public void actualizar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		motiOriDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteMotivoOrigen(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		try {
			return getById(motiOri.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<MotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		return motiOriDao.listAllByEstado();
	}

}
