package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "frecuencias" database table.
 * 
 */
@Entity
@Table(name="\"frecuencias\"")
@NamedQueries({
@NamedQuery(name="Frecuencia.findAll", query="SELECT f FROM Frecuencia f"),
@NamedQuery(name="Frecuencia.findByEstadoAll", query="SELECT f FROM Frecuencia f where f.estado=true")
})
public class Frecuencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Descripcion\"")
	private String descripcion;

	@Column(name="\"Estado\"")
	private Boolean estado;

	public Frecuencia() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}