package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.TipoGastoDao;
import py.com.local.control.money.core.model.TipoGasto;
import py.com.local.control.money.core.services.TipoGastoService;

@Service("tipoGastoService")
@Transactional
public class TipoGastoServiceImpl implements TipoGastoService {

	@Autowired
	private TipoGastoDao tipGasDao;
	
	@Override
	public List<TipoGasto> listarTodos() {
		// TODO Auto-generated method stub
		return tipGasDao.listarTodos();
	}

	@Override
	public Optional<TipoGasto> getById(long id) {
		// TODO Auto-generated method stub
		return tipGasDao.getById((long) id);
	}

	@Override
	public Optional<TipoGasto> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		if (tipoGasto.getId() == null)
			tipGasDao.insertar(tipoGasto);
		else
			tipGasDao.actualizar(tipoGasto);

	}

	@Override
	public void actualizar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		tipGasDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteTipoGasto(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		try {
			return getById(tipoGasto.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<TipoGasto> listAllByEstado() {
		// TODO Auto-generated method stub
		return tipGasDao.listAllByEstado();
	}

}
