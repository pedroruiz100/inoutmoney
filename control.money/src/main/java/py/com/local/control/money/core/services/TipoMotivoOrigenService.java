package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.TipoMotivoOrigen;

public interface TipoMotivoOrigenService {

	List<TipoMotivoOrigen> listarTodos();

	Optional<TipoMotivoOrigen> getById(long id);

	Optional<TipoMotivoOrigen> getByDescri(String descri);

	void insertar(TipoMotivoOrigen tipoMotiOri);

	void actualizar(TipoMotivoOrigen tipoMotiOri);

	void borrarPorId(Long id);
	
	boolean isExisteTipMotiOri(TipoMotivoOrigen tipoMotiOri);
	
	List<TipoMotivoOrigen> listAllByEstado();
}
