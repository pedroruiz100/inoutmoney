package py.com.local.control.money.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;



/**
 * The persistent class for the "movimientos" database table.
 * 
 */
@Entity
@Table(name="\"movimientos\"")
//@NamedQuery(name="Movimiento.findAll", query="SELECT m FROM Movimiento m")
@NamedQueries({
@NamedQuery(name="Movimiento.findAll", query="SELECT m FROM Movimiento m"),
@NamedQuery(name="Movimiento.findByEstadoAll", query="SELECT m FROM Movimiento m where m.estado=true")
})
public class Movimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Descripcion\"")
	private String descripcion;

	@Column(name="\"fecha\"")
	private Timestamp fecha;

	//uni-directional many-to-one association to Frecuencia
	@ManyToOne
	@JoinColumn(name="\"id_frecuencia\"")
	private Frecuencia frecuencia;

	//uni-directional many-to-one association to MotivoOrigenDet
	@ManyToOne
	@JoinColumn(name="\"id_motivo_origen_det\"")
	private MotivoOrigenDet motivoOrigenDet;

	//uni-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="\"id_usuario\"")
	private Usuario usuario;

	@Column(name="\"monto\"")
	private BigDecimal monto;
	
	@Column(name="\"Estado\"")
	private Boolean estado;
	
	@ManyToMany
	@NotEmpty
	@JoinTable(
	name="mov_tipo_presu_det"
	, joinColumns={
	@JoinColumn(name="id_movimiento")
	}
	, inverseJoinColumns={
	@JoinColumn(name="id_tipo_presupuesto")
	}
	)

	private List<TipoPresupuesto> tipoPresu;

	public Movimiento() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}
	
	public Frecuencia getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(Frecuencia frecuencia) {
		this.frecuencia = frecuencia;
	}

	public MotivoOrigenDet getMotivoOrigenDet() {
		return motivoOrigenDet;
	}

	public void setMotivoOrigenDet(MotivoOrigenDet motivoOrigenDet) {
		this.motivoOrigenDet = motivoOrigenDet;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<TipoPresupuesto> getTipoPresu() {
		return tipoPresu;
	}

	public void setTipoPresu(List<TipoPresupuesto> tipoPresu) {
		this.tipoPresu = tipoPresu;
	}

}