package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.TipoGastoDao;
import py.com.local.control.money.core.model.TipoGasto;

@Repository("tipoGastoDao")
public class TipoGastoDaoImpl extends AbstractDao<TipoGasto> implements TipoGastoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoGasto> listarTodos() {
		// TODO Auto-generated method stub
		List<TipoGasto> ti = getEntityManager().createNamedQuery("TipoGasto.findAll").getResultList();
		return ti;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoGasto> listAllByEstado() {
		// TODO Auto-generated method stub
		List<TipoGasto> tp = getEntityManager().createNamedQuery("TipoGasto.findByEstadoAll").getResultList();
		return tp;
	}
	
	@Override
	public Optional<TipoGasto> getById(long id) {
		// TODO Auto-generated method stub
		Optional<TipoGasto> ti = super.getById(id);
		return ti;
	}

	@Override
	public Optional<TipoGasto> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		super.persistir(tipoGasto);

	}

	@Override
	public void actualizar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		super.actualizar(tipoGasto);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		TipoGasto tg = (TipoGasto) getEntityManager().createQuery("DELETE FROM TipoGasto WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

}
