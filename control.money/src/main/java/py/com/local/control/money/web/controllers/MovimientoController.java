package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.services.MovimientoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/movimiento")
@CrossOrigin
public class MovimientoController {

	public static final Logger logger = LoggerFactory.getLogger(MotivoOrigenController.class);

	@Autowired
	private MovimientoService movSrv;

	// ================ RECUPERAMOS TODOS LOS MovimientoS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovimiento() {
		List<Movimiento> mov = movSrv.listarTodos();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(mov, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODOS LOS MOVIMIENTOS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovActivo() {
		List<Movimiento> mov = movSrv.listAllByEstado();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Movimiento A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMovimiento(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Movimiento con id {}.", id);
		Optional<Movimiento> x = movSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Movimiento con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Movimiento con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Movimiento>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN Movimiento ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearMovimiento(@RequestBody Movimiento mov, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Movimiento : {}", mov);
		if (movSrv.isExisteMovimiento(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Movimiento {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Movimiento " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/movimiento/{id}").buildAndExpand(mov.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Movimiento ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarMovimiento(@RequestBody Movimiento mov) {
		logger.info("Actualizando el Movimiento con id {}", mov.getId());
		Optional<Movimiento> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Movimiento con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Movimiento con el id " + mov.getId()), HttpStatus.NOT_FOUND);
		}
		Movimiento movBD = u.get();
		movBD.setDescripcion(mov.getDescripcion());
		movBD.setFecha(mov.getFecha());
		movBD.setFrecuencia(mov.getFrecuencia());
		movBD.setMonto(mov.getMonto());
		movBD.setMotivoOrigenDet(mov.getMotivoOrigenDet());
		movBD.setTipoPresu(mov.getTipoPresu());
		movBD.setUsuario(mov.getUsuario());
		movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		return new ResponseEntity<Movimiento>(movBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Movimiento ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarMovimiento(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Movimiento con id {}", id);
		Optional<Movimiento> mov = movSrv.getById(id);
		if (!mov.isPresent()) {
			logger.error("Eliminación fallida. No existe el Movimiento con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el Movimiento con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		movSrv.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
