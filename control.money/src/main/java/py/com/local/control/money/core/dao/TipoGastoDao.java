package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.TipoGasto;

public interface TipoGastoDao {

	List<TipoGasto> listarTodos();

	Optional<TipoGasto> getById(long id);

	Optional<TipoGasto> getByDescri(String descri);

	void insertar(TipoGasto tipoGasto);

	void actualizar(TipoGasto tipoGasto);

	void borrarPorId(Long id);
	
	List<TipoGasto> listAllByEstado();
}
