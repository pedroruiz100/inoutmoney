package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Usuario;
import py.com.local.control.money.core.services.UsuarioService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class UsuarioController {

	public static final Logger logger = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	private UsuarioService usuarioService;

	// ================ RECUPERAMOS TODOS LOS USUARIOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarUsuarios() {
		List<Usuario> usuarios = usuarioService.listarTodos();
		if (usuarios.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS USUARIOS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarUsuariosActivos() {
		List<Usuario> usuarios = usuarioService.listAllByEstado();
		if (usuarios.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS UN USUARIO A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUsuario(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Usuario con id {}.", id);
		Optional<Usuario> x = usuarioService.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún usuario con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Usuario con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Usuario>(x.get(), HttpStatus.OK);// ver la x
		}

	}
	
	// ================ RECUPERAMOS UN USUARIO A PARTIR DE SU USUARIO Y CLAVE ================
	@RequestMapping(value = "/{usu}/{clave}", method = RequestMethod.GET)
	public ResponseEntity<?> getUsuClave(@PathVariable("usu") String usu, @PathVariable("clave") String clave) {
		Usuario x = usuarioService.getByUsuClave(usu, clave);
		if (x == null) {
			logger.error("No se encontró ningún usuario con Usuario {}.", usu);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Usuario con Usuario " + usu),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Usuario>(x, HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN USUARIO ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearUsuario(@RequestBody Usuario usuario, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Usuario : {}", usuario);
		if (usuarioService.isExisteUsuario(usuario)) {
			logger.error("Inserción fallida. Ya existe un registro con el usuario {}", usuario.getUsuario());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el usuario " + usuario.getUsuario()),
					HttpStatus.CONFLICT);
		}
		usuarioService.insertar(usuario);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/usuario/{id}").buildAndExpand(usuario.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN USUARIO ================
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarUsuario(@RequestBody Usuario usuario) {
		logger.info("Actualizando el Usuario con id {}", usuario.getId());
		Optional<Usuario> u = usuarioService.getById(usuario.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el usuario con el id {}.", usuario.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el usuario con el id " + usuario.getId()), HttpStatus.NOT_FOUND);
		}
		Usuario usuarioBD = u.get();
		usuarioBD.setApellido(usuario.getApellido());
		usuarioBD.setNombre(usuario.getNombre());
		usuarioBD.setCorreo(usuario.getCorreo());
		usuarioBD.setClave(usuario.getClave());
		usuarioBD.setDocumento(usuario.getDocumento());
		usuarioBD.setPerfiles(usuario.getPerfiles());
		usuarioBD.setEstado(usuario.isEstado());
		usuarioService.insertar(usuarioBD);
		return new ResponseEntity<Usuario>(usuarioBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN USUARIO ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarUsuario(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Usuario con id {}", id);
		Optional<Usuario> usuario = usuarioService.getById(id);
		if (!usuario.isPresent()) {
			logger.error("Eliminación fallida. No existe el usuario con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el usuario con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		usuarioService.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// ================ ELIMINAMOS TODOS LOS USUARIOS ================
//	@RequestMapping(value = "/", method = RequestMethod.DELETE)
//	public ResponseEntity<?> eliminarUsuarios() {
//		logger.info("Borrando todos los usuarios");
//		usuarioService.eliminarTodos();
//		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//	}

}
