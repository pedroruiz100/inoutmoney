package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.TipoPresupuestoDao;
import py.com.local.control.money.core.model.TipoPresupuesto;

@Repository("tipoPresupuestoDao")
public class TipoPresupuestoDaoImpl extends AbstractDao<TipoPresupuesto> implements TipoPresupuestoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoPresupuesto> listarTodos() {
		List<TipoPresupuesto> tp = getEntityManager()
				.createNamedQuery("TipoPresupuesto.findAll")
				.getResultList();
		return tp;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoPresupuesto> listAllByEstado() {
		// TODO Auto-generated method stub
		List<TipoPresupuesto> tp = getEntityManager().createNamedQuery("TipoPresupuesto.findByEstadoAll").getResultList();
		return tp;
	}
	
	@Override
	public Optional<TipoPresupuesto> getById(long id) {
		// TODO Auto-generated method stub
		Optional<TipoPresupuesto> tp = super.getById(id);
		return tp;
	}

	@Override
	public Optional<TipoPresupuesto> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoPresupuesto tipoPresu) {
		// TODO Auto-generated method stub
		super.persistir(tipoPresu);

	}

	@Override
	public void actualizar(TipoPresupuesto tipoPresu) {
		// TODO Auto-generated method stub
		super.actualizar(tipoPresu);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		TipoPresupuesto tp = (TipoPresupuesto) getEntityManager().createQuery("DELETE FROM TipoPresupuesto WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

}
