package py.com.local.control.money.core.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.MovimientoDao;
import py.com.local.control.money.core.model.Movimiento;

@Repository("movimientoDao")
public class MovimientoDaoImpl extends AbstractDao<Movimiento> implements MovimientoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Movimiento> listarTodos() {
		// TODO Auto-generated method stub
		List<Movimiento> mo = getEntityManager().createNamedQuery("Movimiento.findAll").getResultList();
		return mo;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Movimiento> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Movimiento> mo = getEntityManager().createNamedQuery("Movimiento.findByEstadoAll").getResultList();
		return mo;
	}

	@Override
	public Optional<Movimiento> getById(long id) {
		// TODO Auto-generated method stub
		Optional<Movimiento> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<Movimiento> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Optional<Movimiento> getByFecha(Timestamp fecha) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Movimiento> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		logger.debug("Fecha: " + fecha);
		try{
			Optional<Movimiento> u = Optional.ofNullable((Movimiento) getEntityManager()
			.createQuery("SELECT u FROM Movimiento u WHERE u.fecha = :fecha")
			.setParameter("Fecha", fecha)
			.getSingleResult());
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
			}catch(NoResultException ex){
			return Optional.empty();
		}
	}
	
	@Override
	public void insertar(Movimiento movimiento) {
		// TODO Auto-generated method stub
		super.persistir(movimiento);

	}

	@Override
	public void actualizar(Movimiento movimiento) {
		// TODO Auto-generated method stub
		super.actualizar(movimiento);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		Movimiento mo = (Movimiento) getEntityManager().createQuery("DELETE FROM Movimiento WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

}
