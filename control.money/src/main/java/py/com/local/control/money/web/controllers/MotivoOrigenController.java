package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.services.MotivoOrigenService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/motivo")
@CrossOrigin
public class MotivoOrigenController {

	public static final Logger logger = LoggerFactory.getLogger(MotivoOrigenController.class);

	@Autowired
	private MotivoOrigenService motiOriSrv;

	// ================ RECUPERAMOS TODOS LOS Motivo ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMotivoOrigen() {
		List<MotivoOrigen> mo = motiOriSrv.listarTodos();
		if (mo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(mo, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODOS LOS MOTIVOS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMotivoActivo() {
		List<MotivoOrigen> mo = motiOriSrv.listAllByEstado();
		if (mo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(mo, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Motivo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMotivo(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el motivo con id {}.", id);
		Optional<MotivoOrigen> x = motiOriSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún motivo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún motivo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<MotivoOrigen>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN MOTIVO ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearMotivo(@RequestBody MotivoOrigen mo, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el motivo : {}", mo);
		if (motiOriSrv.isExisteMotivoOrigen(mo)) {
			logger.error("Inserción fallida. Ya existe un registro con el motivo {}", mo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el motivo " + mo.getId()),
					HttpStatus.CONFLICT);
		}
		motiOriSrv.insertar(mo);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/motivo/{id}").buildAndExpand(mo.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN USUARIO ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarMotivo(@RequestBody MotivoOrigen mo) {
		logger.info("Actualizando el Motivo con id {}", mo.getId());
		Optional<MotivoOrigen> u = motiOriSrv.getById(mo.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el motivo con el id {}.", mo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el motivo con el id " + mo.getId()), HttpStatus.NOT_FOUND);
		}
		MotivoOrigen moBD = u.get();
		moBD.setDescripcion(mo.getDescripcion());
		moBD.setTipoGasto(mo.getTipoGasto());
//		moBD.setDetalles(mo.getDetalles());
		moBD.setEstado(mo.getEstado());
		motiOriSrv.insertar(moBD);
		return new ResponseEntity<MotivoOrigen>(moBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN USUARIO ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarMotivo(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el motivo con id {}", id);
		Optional<MotivoOrigen> mo = motiOriSrv.getById(id);
		if (!mo.isPresent()) {
			logger.error("Eliminación fallida. No existe el motivo con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el motivo con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		motiOriSrv.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
