package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.TipoGasto;
import py.com.local.control.money.core.services.TipoGastoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/tipoGasto")
@CrossOrigin
public class TipoGastoController {

	public static final Logger logger = LoggerFactory.getLogger(TipoGastoController.class);

	@Autowired
	private TipoGastoService tGaServ;

	// ================ RECUPERAMOS TODOS LOS Tipo ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGasto() {
		List<TipoGasto> tg = tGaServ.listarTodos();
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoGasto>>(tg, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODOS LOS TIPO ACTIVO ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoActivo() {
		List<TipoGasto> tg = tGaServ.listAllByEstado();
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoGasto>>(tg, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Tipo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTipoGasto(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Tipo con id {}.", id);
		Optional<TipoGasto> x = tGaServ.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Tipo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Tipo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<TipoGasto>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN Tipo ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearTipoGasto(@RequestBody TipoGasto tg, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Tipo : {}", tg);
		if (tGaServ.isExisteTipoGasto(tg)) {
			logger.error("Inserción fallida. Ya existe un registro con el Tipo {}", tg.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Tipo " + tg.getId()),
					HttpStatus.CONFLICT);
		}
		tGaServ.insertar(tg);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tipogasto/{id}").buildAndExpand(tg.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Tipo ================
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarTipoGasto(@RequestBody TipoGasto tg) {
		logger.info("Actualizando el Tipo con id {}", tg.getId());
		Optional<TipoGasto> u = tGaServ.getById(tg.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Tipo con el id {}.", tg.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Tipo con el id " + tg.getId()), HttpStatus.NOT_FOUND);
		}
		TipoGasto tGBD = u.get();
		tGBD.setDescripcion(tg.getDescripcion());
		tGBD.setEstado(tg.getEstado());
		tGaServ.insertar(tGBD);
		return new ResponseEntity<TipoGasto>(tGBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Tipo ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoGasto(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Tipo con id {}", id);
		Optional<TipoGasto> tg = tGaServ.getById(id);
		if (!tg.isPresent()) {
			logger.error("Eliminación fallida. No existe el tipo con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el tipo con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		tGaServ.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
